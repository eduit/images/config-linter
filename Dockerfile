FROM debian:10
COPY config-lint /usr/local/bin/config-lint
COPY rules /usr/local/share/rules
# RUN apk add git
RUN adduser linter && chmod a+rx /usr/local/bin/config-lint
USER linter
CMD ["/usr/local/bin/config-lint"] 
